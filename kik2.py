import tkinter as tk
from tkinter import ttk
import random

whose_move = None
player_sign = None
computer_sign = None
game_over = False

def open_menu():
    forget_all()
    frame1.pack()

def open_game():
    game_restart()
    forget_all()
    frame2.pack()
    global whose_move
    global player_sign
    global computer_sign
    whose_move = combobox_who_is_first.get()
    player_sign = combobox_your_sign.get()
    if player_sign == "O":
        computer_sign = "X"
    else:
        computer_sign = "O"

    if whose_move == "Komputer":
        make_computer_move()

    window.mainloop()

def make_computer_move():
    if game_ends():
        return 0
    print("computer move")
    label.configure(text="Gdzie by tu ruszyć... Może tu. Twój ruch.")
    global computer_sign
    fields = {
        1:button1,
        2:button2,
        3:button3,
        4:button4,
        5:button5,
        6:button6,
        7:button7,
        8:button8,
        9:button9
        }
    while True:
        num = random.randint(1,9)
        if fields[num].cget("text") == "":
            fields[num].configure(text=computer_sign)
            break
    if game_ends():
        return 0
    
def game_restart():
    global game_over
    game_over = False
    button1.configure(text="")
    button2.configure(text="")
    button3.configure(text="")
    button4.configure(text="")
    button5.configure(text="")
    button6.configure(text="")
    button7.configure(text="")
    button8.configure(text="")
    button9.configure(text="")
    label.configure(text="Twój ruch.")

def game_ends():
    global game_over
    if ((button1.cget("text") == button2.cget("text") == button3.cget("text") == "O") or
	(button4.cget("text") == button5.cget("text") == button6.cget("text") == "O") or
	(button7.cget("text") == button8.cget("text") == button9.cget("text") == "O") or
	(button1.cget("text") == button4.cget("text") == button7.cget("text") == "O") or
	(button2.cget("text") == button5.cget("text") == button8.cget("text") == "O") or
	(button3.cget("text") == button6.cget("text") == button9.cget("text") == "O") or
	(button1.cget("text") == button5.cget("text") == button9.cget("text") == "O") or
	(button3.cget("text") == button5.cget("text") == button7.cget("text") == "O")):
        game_over = True
        label.configure(text="Koniec gry. Wygrało kółko.")
        return True
    if ((button1.cget("text") == button2.cget("text") == button3.cget("text") == "X") or
	(button4.cget("text") == button5.cget("text") == button6.cget("text") == "X") or
	(button7.cget("text") == button8.cget("text") == button9.cget("text") == "X") or
	(button1.cget("text") == button4.cget("text") == button7.cget("text") == "X") or
	(button2.cget("text") == button5.cget("text") == button8.cget("text") == "X") or
	(button3.cget("text") == button6.cget("text") == button9.cget("text") == "X") or
	(button1.cget("text") == button5.cget("text") == button9.cget("text") == "X") or
	(button3.cget("text") == button5.cget("text") == button7.cget("text") == "X")):
        label.configure(text="Koniec gry. Wygrał krzyżyk.")
        game_over = True
        return True
    if (button1.cget("text") != "" and
        button2.cget("text") != "" and
        button3.cget("text") != "" and
        button4.cget("text") != "" and
        button5.cget("text") != "" and
        button6.cget("text") != "" and
        button7.cget("text") != "" and
        button8.cget("text") != "" and
        button9.cget("text") != ""
        ):
        label.configure(text="Koniec gry. Remis.")
        game_over = True
        return True
    else:
        return False
    
def forget_all():
    try:
        frame1.pack_forget()
    except:
        pass
    try:
        frame2.pack_forget()
    except:
        pass

def button1_game_board_click(event):
    global game_over
    print("game_over value is:", game_over)
    if button1.cget("text") == "" and game_over == False:
        button1.configure(text=player_sign)
        if not game_ends():
            make_computer_move()
def button2_game_board_click(event):
    global game_over
    if button2.cget("text") == "" and game_over == False:
        button2.configure(text=player_sign)
        if not game_ends():
            make_computer_move()
def button3_game_board_click(event):
    global game_over
    if button3.cget("text") == "" and game_over == False:
        button3.configure(text=player_sign)
        if not game_ends():
            make_computer_move()
def button4_game_board_click(event):
    global game_over
    if button4.cget("text") == "" and game_over == False:
        button4.configure(text=player_sign)
        if not game_ends():
            make_computer_move()
def button5_game_board_click(event):
    global game_over
    if button5.cget("text") == "" and game_over == False:
        button5.configure(text=player_sign)
        if not game_ends():
            make_computer_move()
def button6_game_board_click(event):
    global game_over
    if button6.cget("text") == "" and game_over == False:
        button6.configure(text=player_sign)
        if not game_ends():
            make_computer_move()
def button7_game_board_click(event):
    global game_over
    if button7.cget("text") == "" and game_over == False:
        button7.configure(text=player_sign)
        if not game_ends():
            make_computer_move()
def button8_game_board_click(event):
    global game_over
    if button8.cget("text") == "" and game_over == False:
        button8.configure(text=player_sign)
        if not game_ends():
            make_computer_move()
def button9_game_board_click(event):
    global game_over
    if button9.cget("text") == "" and game_over == False:
        button9.configure(text=player_sign)
        if not game_ends():
            make_computer_move()

window = tk.Tk()
window.title("Kółko i Krzyżyk")

#okno MENU GŁÓWNE:
frame1 = tk.Frame()

frame1_upper = tk.Frame(master=frame1, padx=5, pady=5)
frame1_upper.pack()
frame1_upper_left = tk.Frame(master=frame1_upper, padx=5, pady=5)
frame1_upper_left.pack(side=tk.LEFT)
frame1_upper_right = tk.Frame(master=frame1_upper, padx=5, pady=5)
frame1_upper_right.pack(side=tk.LEFT)
frame1_lower = tk.Frame(master=frame1, padx=5, pady=5)
frame1_lower.pack()

label_your_sign = tk.Label(master=frame1_upper_left, text="Twój znak:")
label_your_sign.pack()
label_who_is_first = tk.Label(master=frame1_upper_left, text="Kto pierwszy:")
label_who_is_first.pack()
combobox_your_sign = ttk.Combobox(master=frame1_upper_right)
combobox_your_sign.configure(values=("O","X"), state="readonly")
combobox_your_sign.current(0)
combobox_your_sign.pack()
combobox_who_is_first = ttk.Combobox(master=frame1_upper_right)
combobox_who_is_first.configure(values=("Gracz","Komputer"), state="readonly")
combobox_who_is_first.current(0)
combobox_who_is_first.pack()
button_start = tk.Button(master=frame1_lower, text="START", command=open_game)
button_start.pack(side=tk.LEFT, padx=5, pady=5)
button_exit = tk.Button(master=frame1_lower, text="EXIT", command=window.destroy)
button_exit.pack(side=tk.LEFT, padx=5, pady=5)

#okno GRY:
frame2 = tk.Frame()

frame2_upper = tk.Frame(master=frame2, padx=5, pady=5)
frame2_upper.pack()
frame2_upper_left = tk.Frame(master=frame2_upper, padx=5, pady=5)
frame2_upper_left.pack(side=tk.LEFT)
frame2_upper_left_row1 = tk.Frame(master=frame2_upper_left)
frame2_upper_left_row1.pack()
frame2_upper_left_row2 = tk.Frame(master=frame2_upper_left)
frame2_upper_left_row2.pack()
frame2_upper_left_row3 = tk.Frame(master=frame2_upper_left)
frame2_upper_left_row3.pack()
frame2_upper_right = tk.Frame(master=frame2_upper, padx=5, pady=5)
frame2_upper_right.pack(side=tk.LEFT)
frame2_lower = tk.Frame(master=frame2, padx=5, pady=5)
frame2_lower.pack()

button1 = tk.Button(master=frame2_upper_left_row1, height=2, width=4, font=("Arial", 40))
button1.pack(side=tk.LEFT)
button1.bind("<Button-1>", button1_game_board_click)
button2 = tk.Button(master=frame2_upper_left_row1, height=2, width=4, font=("Arial", 40))
button2.pack(side=tk.LEFT)
button2.bind("<Button-1>", button2_game_board_click)
button3 = tk.Button(master=frame2_upper_left_row1, height=2, width=4, font=("Arial", 40))
button3.pack(side=tk.LEFT)
button3.bind("<Button-1>", button3_game_board_click)
button4 = tk.Button(master=frame2_upper_left_row2, height=2, width=4, font=("Arial", 40))
button4.pack(side=tk.LEFT)
button4.bind("<Button-1>", button4_game_board_click)
button5 = tk.Button(master=frame2_upper_left_row2, height=2, width=4, font=("Arial", 40))
button5.pack(side=tk.LEFT)
button5.bind("<Button-1>", button5_game_board_click)
button6 = tk.Button(master=frame2_upper_left_row2, height=2, width=4, font=("Arial", 40))
button6.pack(side=tk.LEFT)
button6.bind("<Button-1>", button6_game_board_click)
button7 = tk.Button(master=frame2_upper_left_row3, height=2, width=4, font=("Arial", 40))
button7.pack(side=tk.LEFT)
button7.bind("<Button-1>", button7_game_board_click)
button8 = tk.Button(master=frame2_upper_left_row3, height=2, width=4, font=("Arial", 40))
button8.pack(side=tk.LEFT)
button8.bind("<Button-1>", button8_game_board_click)
button9 = tk.Button(master=frame2_upper_left_row3, height=2, width=4, font=("Arial", 40))
button9.pack(side=tk.LEFT)
button9.bind("<Button-1>", button9_game_board_click)

button_main_menu = tk.Button(master=frame2_upper_right, text="Menu główne", font=("Arial", 20), command=open_menu)
button_main_menu.pack(padx=5, pady=5)
button_restart = tk.Button(master=frame2_upper_right, text="Restart", font=("Arial", 20), command=open_game)
button_restart.pack(padx=5, pady=5)
button_exit = tk.Button(master=frame2_upper_right, text="Wyjście", font=("Arial", 20), command=window.destroy)
button_exit.pack(padx=5, pady=5)

label = tk.Label(master=frame2_lower, text="Komunikat")
label.pack(padx=5, pady=5)

open_menu()

window.mainloop()
